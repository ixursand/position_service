package service

import (
	"context"

	"bitbucket.org/Udevs/position_service/genproto/position_service"
	"bitbucket.org/Udevs/position_service/pkg/helper"
	"bitbucket.org/Udevs/position_service/pkg/logger"
	"bitbucket.org/Udevs/position_service/storage"
	"github.com/jmoiron/sqlx"
	"google.golang.org/grpc/codes"
)

type professionService struct {
	logger  logger.Logger
	storage storage.StorageI
}

func NewProfessionService(db *sqlx.DB, log logger.Logger) *professionService {
	return &professionService{
		logger:  log,
		storage: storage.NewStoragePg(db),
	}
}

func (s *professionService) Create(ctx context.Context, req *position_service.Profession) (*position_service.ProfessionId, error) {
	id, err := s.storage.Profession().Create(req)
	if err != nil {
		return nil, helper.HandleError(s.logger, err, "error while create profession", req, codes.Internal)
	}

	return &position_service.ProfessionId{
		Id: id,
	}, nil
}

func (s *professionService) GetAll(ctx context.Context, req *position_service.GetAllProfessionRequest) (*position_service.GetAllProfessionResponse, error) {
	resp, err := s.storage.Profession().GetAll(req)
	if err != nil {
		return nil, helper.HandleError(s.logger, err, "error while getting all professions", req, codes.Internal)
	}

	return resp, nil
}

func (s *professionService) Get(ctx context.Context, req *position_service.ProfessionId) (*position_service.Profession, error) {
	profession, err := s.storage.Profession().Get(req.Id)
	if err != nil {
		return nil, helper.HandleError(s.logger, err, "error while getting  profession", req, codes.Internal)
	}

	return profession, nil
}
